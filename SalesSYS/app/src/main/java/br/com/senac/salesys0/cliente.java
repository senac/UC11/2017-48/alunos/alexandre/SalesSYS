package br.com.senac.salesys0;

import android.os.Bundle;

import java.io.Serializable;

/**
 * Created by sala302b on 01/02/2018.
 */

public class cliente implements Serializable {


    private String nome;
    private String cidade;
    private String uf;
    private String profissao;
    private String empresa;
    private String tel;
    private String email;
    private String obs;


    public cliente(String nome, String cidade, String uf, String profissao, String empresa, String tel, String email, String obs) {
        this.nome = nome;
        this.cidade = cidade;
        this.uf = uf;
        this.profissao = profissao;
        this.empresa = empresa;
        this.tel = tel;
        this.email = email;
        this.obs = obs;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }


}